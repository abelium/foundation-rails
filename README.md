Fork of the zurb's [foundation-rails](https://github.com/zurb/foundation-rails) gem.
Made a patch in 5.0.2.0 version. Branch *foundation-5.0.2.0* shall be used!

Include in Gemfile with the following line: `gem 'foundation-rails', bitbucket: 'abelium/foundation-rails', branch: 'foundation-5.0.2.0'`